import spacy
from spacy.tokens import Doc
from typing import List
from pathlib import Path
import pandas as pd 

def simple_label(text:str, labels:List[Doc], nlp):
    doc = nlp(text)
    matches = [ label.text for label in labels if label.text.strip().lower() in doc.text.lower()]
   
    if matches:
        if matches[0].strip() == 'hershey':
            return 'chocolate'
        if matches[0].strip() == 'toblerone':
            return 'chocolate'
        if matches[0].strip() == 'mars':
            return 'chocolate'
        if matches[0].strip() == 'trident':
            return 'gum'
        if matches[0].strip() == "reese's":
            return "reeses"
        else:
            return matches[0].strip()
    else:        
        similarity = [(label.text, doc.similarity(label)) for label in labels]
        similarity.sort(key = lambda x: x[1])
        if similarity[0][1] > 0.6:
            return similarity[0][0]
        else:
            return None
    



def main():
    df = pd.read_csv('candy-data.csv')
    nlp = spacy.load('en_core_web_lg')
    simple_labels = Path('general_labels.txt').read_text().split('\n')
    for i, label in enumerate(simple_labels):
        if "_" in label:
            simple_labels[i] = ' '.join(label.split('_'))  

    labels = [nlp(label) for label in simple_labels]
    df['simple_label'] = df['name'].apply(lambda x: simple_label(x, labels, nlp))
    df.to_csv('new.csv', index=False)

if __name__ == "__main__":
    main()

