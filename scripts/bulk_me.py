import typer
import pandas as pd
from umap import UMAP

# pip install -U sentence-transformers
from sentence_transformers import SentenceTransformer


def main():
        
    # Load the universal sentence encoder
    model = SentenceTransformer('paraphrase-MiniLM-L6-v2')

    # Load original dataset
    df = pd.read_csv("../candy-data.csv")[['name']]
    df['text'] = df['name']
    # Calculate embeddings 
    X =  model.encode(df['text'])

    # Reduce the dimensions with UMAP
    umap = UMAP(n_components=2)
    X_tfm = umap.fit_transform(X)

    # Apply coordinates
    df['x'] = X_tfm[:, 0]
    df['y'] = X_tfm[:, 1]

    df.to_csv("../bulk.csv", index=False)


if __name__ == "__main__":
    typer.run(main)
