import typer
import csv
import uuid
import json
import srsly
import requests
from multiprocessing.pool import ThreadPool
from pathlib import Path 
from typing import List

# Delete existing folder of images, uuids are unique each run
def rm_tree(pth):
    pth = Path(pth)
    for child in pth.glob('*'):
        if child.is_file():
            child.unlink()
        else:
            rm_tree(child)
    pth.rmdir()

if Path('../images').exists():
    rm_tree('../images')
    Path('../images').mkdir(parents=True, exist_ok=False)
else:
    Path('../images').mkdir(parents=True, exist_ok=False)

json_file = Path('../candy-data.jsonl')
if json_file.exists():
    existing_data = srsly.read_jsonl('../candy-data.jsonl')
else:
    existing_data = False

def load_data():
    data = []
    with open('../candy-data.csv', mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                data.append(row)
                #if row['simple_label']:
                #    data.append(row)
    return data 


def download_url(item:dict):
    url = item.get('image-src', None)
    if not 'http' in url:
        url = 'https:' + url
    if url:
        r = requests.get(url, stream=True)
        if r.status_code == requests.codes.ok:
            if item["simple_label"]:
                with open(f'../images/{item["simple_label"]}/'+ item['id'], 'wb') as f:
                    for data in r:
                        f.write(data)
            else:
                with open('../images/'+ item['id'], 'wb') as f:
                    for data in r:
                        f.write(data)
        return item['id'], url, True
    else:
        return item['id'], url, False

def remove_existing(data: List[dict], existing_data: List[dict]):
    for record in data:
        if record in existing_data:
            data.remove(record)
    return data

def main():
    data = load_data()
    if existing_data:
        data = remove_existing(data, existing_data)
    count = len(data)
    for item in data:
        item['id'] = str(uuid.uuid1())
        if item.get("simple_label", None):
            label_path = Path(f'../images/{item["simple_label"]}/')
            if not label_path.exists():
                label_path.mkdir(parents=True, exist_ok=False)

    results = ThreadPool(5).imap_unordered(download_url, data)
    with open('../candy-data.jsonl','a') as log:
        try:
            for id, url, download in results:
                print(f'🫠 {count}', url, download)
                count -= 1
                item = next((x for x in data if x["id"] == id), None)
                if item:
                    item['download'] = download
                    item_json = json.dumps(item)
                    log.write(item_json + '\n')
        except Exception as e:
            print(e)

if __name__ == "__main__":
    typer.run(main)


 
 
